import { shallowMount } from "@vue/test-utils";
import OpenJscad from "@/components/OpenJscad.vue";

describe("OpenJscad.vue", () => {
  it("renders props.msg when passed", () => {
    const wrapper = shallowMount(OpenJscad, {
      propsData: { design: "gearset.jscad" }
    });

    console.log("wrapper", wrapper.html());
  });
});
