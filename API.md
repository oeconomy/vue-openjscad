# open-jscad 

A Vue.js wrapper for the OpenJSCad UMD module build.

```
<open-jscad
design="gearset.jscad"
:panel="{ size: 223 }"
:camera="{
position: { x: 0, y: 0, z: 150 },
clip: { min: 1, max: 1000 }
}"
></open-jscad>
``` 

## props 

- `design` ***String*** (*required*) 

  Url of the design. This must be a valid OpenJSCAD file. 

- `camera` ***CameraSettings*** (*optional*) `default: undefined` 

  Initial camera settings.
  
  **CameraSettings** Object
  
  * `fov` **number** field of view.
  * `angle` **{x: number, y: number, z: number}** view angle about XYZ axis.
  * `position` **{x: number, y: number, z: number}** initial position at XYZ.
  * `clip` **{min: number, max: number}** rendering outside this range is clipped. 

- `axis` ***AxisSettings*** (*optional*) `default: undefined` 

  You can enable drawing axes and set the colors for each.
  
  **AxisSettings** Object
  
  * `draw` **boolean** draw or not (default: false).
  * `x` **object** x axis color
  - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
  - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0)
  * `x` **object** y axis color
  - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
  - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0)
  * `x` **object** z axis color
  - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
  - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0) 

- `panel` ***PlateSettings*** (*optional*) `default: undefined` 

  The panel is the grid shown in the viewer.  You can disable the grid or customize it.
  
  **PlateSettings** Object
  
  * `draw` **boolean** draw or not (default: false).
  * `size` **number** size of the grid
  * `m` **object** minor grid settings
  - `i` **number** number of units between grid lines.
  - `color` **{r: number, g: number, b: number, a: number}** minor grid color. (values 0.0 - 1.0)
  * `M` **object** major grid settings
  - `i` **number** number of units between grid lines.
  - `color` **{r: number, g: number, b: number, a: number}** major grid color. (values 0.0 - 1.0) 

- `delay` ***Number*** (*optional*) 

## data 

- `formats` 

**initial value:** `undefined` 

- `format` 

**initial value:** `undefined` 

- `outputFile` 

**initial value:** `undefined` 

- `status` 

**initial value:** `undefined` 

- `statusData` 

**initial value:** `undefined` 

## computed properties 

- `size` 

  @description computes teh curent clientWidth and Height 

   **dependencies:** `$el`, `$el` 

- `downloadFileName` 

  @description the download filename 

   **dependencies:** `format`, `design`, `format` 


## methods 

- `setStatus(status, data)` 

  Call back used to set stats from jscadViewer 

  **parameters:** 

     - `status` **any** - jscadViewer processor status. 
     - `data` **any** - Additional data associated with current status. 

- `onUpdate(data)` 

  Hook called when anything changes in the jscadViewer.
  Inside the OpenJSCad, this is updated when `enableItems`
  is called. 

  **parameters:** 

     - `data` **updateInfo** - Updated data 

- `generateFile()` 

  Asynchronously create an output file with the current `format`.  `onUpdate` will
  be set with the outputFile information. 

- `clearOutputFile()` 

  Clear the output file info.  Used when a parameter or file format changes. 

- `resetCamera()` 

  Reset the camera to the home position. 

- `fetchJscadFile()` 

  Use the browsers `fetch` api to retrieve the design. 

   **return value:** 

     - **Any** - {any} 
- `wait(ms)` 

