// vue.config.js
module.exports = {
  chainWebpack: config => {
    config.module
      .rule("md")
      .test(/\.md$/)
      .use("vue-loader")
      .loader("vue-loader")
      .end()
      .use(["vue-loader", "vmark-loader"])
      .loader("vmark-loader")
      .end();

    config.externals(
      process.env.NODE_ENV === "production" && !process.env.DEMOAPP
        ? {
            ...config.get("externals"),
            "@jscad/web": "@jscad/web",
            debug: "debug"
          }
        : { ...config.get("externals") }
    );
  }
};
